


module Main exposing (..)

import Date exposing (Date)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)




main =
  Html.program
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    }


-- MODEL

type alias Model =
    {
      endDate : String,
      minutes : String,
      startDate : String
    }



-- INIT


init : ( Model, Cmd Msg )
init =
    ( Model "" "" "", Cmd.none )



-- UPDATE


type Msg
    = UpdateMinutes String
    | UpdateStart   String
    | Submit

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        UpdateMinutes minutes ->
            ({model | minutes = minutes}, Cmd.none)

        UpdateStart startDate  ->
            ({model | startDate = startDate}, Cmd.none)

        submit ->
            ({model | endDate = getEndDate model.minutes} , Cmd.none )
-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ h1 [ ] [text "Time Tracker"]
        , input  [type_ "text", placeholder "Enter Start Date", onInput UpdateStart ] []
        , input  [type_ "text", placeholder "Enter Task minutes", onInput UpdateMinutes ] []
        , button [ onClick Submit ] [ text "Submit" ]
        , br [] []
        , text model.endDate
        ]



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


getEndDate : String -> String
getEndDate date =
    ""