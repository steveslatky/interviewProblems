#!/usr/bin/python3


# List of error codes
# -1 == Invalid startDate format entered


import datetime

def main():
    date = datetime.datetime
    start = input("Please enter the start date of the task in" + 
            "the correct format \n" + 
            "(E.g. Monday, January 01, 2018 06:00:00 AM): ")
    
    minutes = 0
    while minutes <= 0:     
        try:
            minutes = int(input("How many minutes will the task take?" + 
                          " Value must be over 0:  "))
        except ValueError: 
            print("That is not a valid number silly. Try again")

    print(getEndDate(start, minutes).strftime("%A, %B %d, %Y %I:%M:%S %p"))

# String -> int -> datetime
def getEndDate(startDate, minutes):
    startDate = getDateTime(startDate)
    convertedTime = convertMinutes(minutes)
    endDate = findEndDate(startDate, convertedTime)

    return endDate


# datetime -> (int,int,int) -> datetime
def findEndDate(date, c_time):
    # Will use to add minutes to the time.
    td_min = datetime.timedelta(minutes = c_time[2])

    # Going smallest to largest time amount lets the invalid
    # day/time checking work
    date = date + td_min

    # Needed if only has minutes and is not during work hours.
    if c_time[1] == 0 and not isWorkHours(date):
        date = findHour(date,1)

    # Advance hours
    date = findHour(date,c_time[1])

    # Needed if minutes entered was less than a day
    # and day is not on a workday.
    if c_time[0] == 0 and not isWorkDay(date):
        date = findDay(date, 1)

    date = findDay(date,c_time[0])

    return date


# datetime -> int -> datetime
def findHour(dt, hours):
    td_hour= datetime.timedelta(hours = 1)

    while hours is not 0:
        dt = dt + td_hour
        if isWorkHours(dt):
            hours -= 1
    return dt


# datetime -> int -> datetime
def findDay(dt, days):
    td_day = datetime.timedelta(days = 1)

    while days is not 0:
        dt = dt + td_day
        if isWorkDay(dt):
           days -= 1
    return dt


# String -> datetime
def getDateTime(startDate):
    date = datetime.datetime
    try:
        date = date.strptime(startDate , "%A, %B %d, %Y %I:%M:%S %p")
    except ValueError:
        print("Please enter the correct date format for the start time.\n" +
                "E.g. Wednesday, January 03, 2018 06:00:00 AM")
        exit(-1)
    return date


# int -> (int,int,int)
# Takes in minutes converts to (days, hours, minutes)
def convertMinutes(minutes):
    days = int(minutes / 480)
    minutes = minutes % 480

    hours = int(minutes / 60)
    minutes = minutes % 60

    return(days,hours,minutes)


# datetime -> bool
# Checks to see if day is on the weekend or a holiday
def isWorkDay(dt):
    day = dt.weekday()
    # 5 == Saturday , 6 == Sunday
    if day is 5 or day is 6 or isHoliday(dt):
        return False
    else: return True


# datetime -> bool
# Checks within work hours and not lunch time.
def isWorkHours(dt):
    # Make sure it is not after 17:00
    if dt.hour is 17 and dt.minute is 0:
        return True
    elif dt.hour >= 8 and dt.hour < 17 and dt.hour is not 12:
        return True
    else: return False


# datetime -> bool
def isHoliday(dt):
    # Christmas
    if dt.month == 12 and dt.day == 25:
        return True
    # New years Day
    if dt.month == 1  and dt.day == 1 :
        return True
    # 4th of July
    if dt.month == 7  and dt.day == 4 :
        return True
    if findThanksgiving(dt.year) == dt.date():
        return True
    if findMemorialDay(dt.year) == dt.date(): 
        return True
    if findLaborDay(dt.year) == dt.date(): 
        return True

    return False


# int -> datetime
# find the first Thursday 
# add 3 weeks to get the 4th thursday
def findThanksgiving(year):
    d = datetime.date(year, 11, 1)

    td_day = datetime.timedelta(days = 1)
    td_3weeks = datetime.timedelta(days = 21) 
    while d.weekday() is not 3: 
        d = d + td_day
    return d + td_3weeks


# int -> datetime
# Start at the end of May
# move back 1 day until you find monday
def findMemorialDay(year):
    d = datetime.date(year, 5, 31)
    
    td_day = datetime.timedelta(days = 1)
    while d.weekday() is not 0:
        d = d - td_day
    return d


# int -> datetime
# Start at the begining of September
# move foreward 1 day until you find monday
def findLaborDay(year): 
    d = datetime.date(year, 9, 1)
    
    td_day = datetime.timedelta(days = 1)
    while d.weekday() is not 0:
        d = d + td_day
    return d



if __name__ == "__main__":
    main()
