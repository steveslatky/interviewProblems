import unittest
from main import *

class TestQuestions(unittest.TestCase):
    def q_1():
        start = "Wednesday, January 03, 2018 06:00:00 AM"
        minutes = 55
        answer = "Wednesday, January 03, 2018 06:55:00 AM"
        self.assertEquals(getEndDate(start, minutes) == answer)


if __name__ == '__main__':
    unittest.main()
