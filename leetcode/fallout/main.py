#!/usr/bin/python3
import collections
import copy

class Node:
    def __init__(self, cargo, parent=None, key="", state=None, char_found=0):
        self.move = cargo
        self.childern = []
        self.parent = parent
        self.state = state
        self.key = key
        self.char_found = char_found

    def is_solved(self):
        return self.char_found == len(self.key)

    @property
    def __str__(self):
        return str(self.cargo)

class Solution:
    def findRotateSteps(self, ring, key):
        """
        :type ring: str
        :type key: str
        :rtype: int
        """
        ans = start_bfs(ring, key) + len(key)
        print(ans)
        return ans

def start_bfs(ring, key):
    root = init_tree(ring, key)
    open_list = collections.deque()
    open_list.appendleft(root)
    final_nodes = []

    while open_list:
        # Get first item in queue
        current_node = open_list.popleft()
        # Check if game is solved
        #if current_node.is_solved():
        #    return transverse_tree(current_node)
        if current_node.is_solved():
            final_nodes.append(current_node)
            continue

        set_childern_node(current_node)
        for child in current_node.childern:
            open_list.append(child)

    return find_min(final_nodes)

def find_min(nodes):
    min_node = 999999999
    for node in nodes:
        c = transverse_tree_no_print(node)
        if c < min_node:
            min_node = c
    return min_node

def apply_move_clone(state, move):
    c_state = copy.deepcopy(state)
    if move[1] is 0:
        return c_state
    if move[0] is "right":
        tmp = c_state[:move[1]] # Gets everything before letter in key
        c_state = c_state[move[1]:] + tmp
    else:
        # 1 need to be taken off from the move count
        # When reverseing array, it moves 1 ccw
        c_state = c_state[::-1]
        tmp = c_state[:move[1] - 1]
        c_state = c_state[move[1] - 1:] + tmp

    return c_state


def init_tree(state, key):
    return Node(None, None, key, state)  # Initial state has no move and no parent.


# Tracks up a tree to print out moves
def transverse_tree_no_print(node):
    count = 0
    finished_node = copy.copy(node)
    while node.parent is not None:
        count += node.move[1]
        node = node.parent

    return count

# Tracks up a tree to print out moves
def transverse_tree(node):
    moves = []
    count = 0
    finished_node = copy.copy(node)
    while node.parent is not None:
        moves.append(node.move)
        node = node.parent
    for move in reversed(moves):
        count += move[1]

    return count


# Finds the number of moves away 1 direction is.
def get_all_moves(node):
    if len(node.key) is node.char_found: return None
    if node.state[0] is node.key[node.char_found]:
        return [("left", 0), ("right", 0)]

    state = copy.deepcopy(node.state)
    for e, i in enumerate(state):
        if i is node.key[node.char_found]:
            right_path = e
            break

    state = copy.deepcopy(node.state)
    for e, i in enumerate(state[::-1]):
        if i is node.key[node.char_found]:
            left_path = e + 1
            break

    return [("left", left_path), ("right", right_path)]


def set_childern_node(node):
    moves = get_all_moves(node)
    if moves is None:
        return
    for direction in moves: # (direction, num)
        node.childern.append(
            Node(direction, node, node.key, apply_move_clone(node.state, direction), node.char_found + 1))


if __name__ == "__main__":
    z = Solution()

    z.findRotateSteps("jrlucwzakzussrlckyjjsuwkuarnaluxnyzcnrxxwruyr", "xrrakuulnczywjs")
